---
title: 最大子序列
toc: true
date: 2022/09/20
update: 2022/09/28
categories:
  - 计算之魂
tags:
  - 计算之魂
  - 算法
---
# 问题描述

给定一个实数序列，设计一个最有效的算法，找到一个总和最大的区间。

```
1.5,-12.3,3.2,-5.5,23.2,3.2,-1.4,-12.2,34.2,5.4,-7.8,1.1,-4.9
```

书中总共给出了4中解决方法，第一种最差，第二种比第一种优秀，以此类推，第四种最好。

<!--more-->

# 解题方法

这里我们采用Java语言实现。首先定义一个存储子序列信息的对象。

```java
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Subsequence {

    /**
     * 原始数组
     */
    private double[] original;

    /**
     * 开始位置
     */
    private int start = -1;

    /**
     * 结束位置
     */
    private int end = -1;

    /**
     * 子序列之和
     */
    private double sum;


    /**
     * 打印子序列信息
     */
    public void print() {
        for (int i = 0; i < original.length; i++) {
            if (i == start) {
                System.out.print("start[");
            }
            System.out.print(original[i]);
            if (i == end) {
                System.out.print("]end");
            }
            System.out.print("\t");
            if (i != 0 && i % 10 == 0) {
                System.out.println();
            }
        }
        System.out.println();
        System.out.println("S(" + start + "," + end + ") = " + sum);
    }

}
```

## 方法1，做一次三重循环

其实就是中学中的排列组合的方法。

我们假设这个序列有$K$ 个数，依次是 $a_1,a_2,\dots,a_k$。假定区间起始的数字序号为 $p$ ，结束的序号为 $q$ ，这些数字的总和为 $S(p,q)$ ，则

$$
S(p,q) = \sum_{i=p}^q{a_i}=a_p+a_{p+1}+\dots+a_q
$$

$p$ 可以从 $1$ 一直到 $K$,$q$ 可以从 $p$ 一直到 $K$，这就是两重循环了，因此区间一头一尾的组合有 $O(K^2)$ 中<sup>①</sup>。在每一种组合中，计算 $S(p,q)$ 平均要做 $K/4$ 次加法，这是又一重循环。因此这种算法的复杂度是 $O(K^3)$<sup>②</sup>

> 由于题中定义的序列中元素的个数用 $K$ 表示，所以我们在后文中用于表示时间复杂度是都采用 $O(K)$ 这种形式，而不是 $O(n)$。

**代码实现参考（Java语言）**

首先写一个方法用于计算 $S(p,q)$(与②对应)

```java
/**
 * 计算 S(p,q)
 */
private double sum(double[] nums, int start, int end) {
    double sum = 0.0;
    for (int i = start; i <= end; i++) {
        sum += nums[i];
    }
    return sum;
}
```

然后再通过两重循环找到最大的子序列。(与①对应)
我们定义三个变量，`max,start,end`，分别记录当前最大的数，当前最大数的开始位置和结束位置。在比较结束之后，变量中存储的数就是我们要找的结果。

```
public Subsequence findOut(double[] nums) {
    double max = Double.MIN_VALUE;
    int start = -1;
    int end = -1;
    for (int i = 0; i < nums.length; i++) {
        for (int j = i; j < nums.length; j++) {
            double sum = this.sum(nums, i, j);
            // 比较sum和max如果sum大于max，则修改start和end的位置，同时将sum赋值给max
            if (sum > max) {
                max = sum;
                start = i;
                end = j;
            }
        }
    }
    return new Subsequence(nums, start, end, max);
}
```

完整代码如下：

```java
public class MaxSubsequence {

    public static void main(String[] args) {
        double[] arrs = {1.5, -12.3, 3.2, -5.5, 23.2, 3.2, -1.4, -12.2, 34.2, 5.4, -7.8, 1.1, -4.9};
        MaxSubsequence maxSubsequence = new MaxSubsequence();
        Subsequence subsequence = maxSubsequence.findOut(arrs);
        subsequence.print();
    }

    public Subsequence findOut(double[] nums) {
        double max = Double.MIN_VALUE;
        int start = -1;
        int end = -1;
        for (int i = 0; i < nums.length; i++) {
            for (int j = i; j < nums.length; j++) {
                double sum = this.sum(nums, i, j);
                if (sum > max) {
                    max = sum;
                    start = i;
                    end = j;
                }
            }
        }
        return new Subsequence(nums, start, end, max);
    }

    /**
     * 计算 S(p,q)
     */
    private double sum(double[] nums, int start, int end) {
        double sum = 0.0;
        for (int i = start; i <= end; i++) {
            sum += nums[i];
        }
        return sum;
    }

}
```

输出结果

```
1.5	-12.3	3.2	-5.5	start[23.2	3.2	-1.4	-12.2	34.2	5.4]end	-7.8
1.1	-4.9
S(4,9) = 52.4
```

## 方法二 做两重循环

如果计算了$S(p,q)$，下次计算 $S(p,q+1)$，则只需计算 $S(p,q) + a_{q+1}$ 即可。这样计算$S(p,q)$就无需再做一次循环。

只需要记录这样三个中间值。

- 第一个是从$p$开始到当前位置$q$为止的总和$S(p,q)$，因为我们接下来计算$S(p,q+1)$时会用到。
- 第二个值则是从$p$开始到当前位置$q$为止所有总和中最大的那个值，我们假定为$max$。若$S(p,q+1) <= max$ 则 $max$ 不变，否则 $max = S(p, q+1)$。
- 第三个值就是区间结束的位置，我们不妨以$r$ 表示。如果 $max$的值更新了，相应的区间结束位置也要更新为$q+1$

方法二是对方法一的优化，在方法一种，如果若计算 $S(p,q+1)$的值，是通过 $q - p$ 次加法运算实现的，而方法二中只需一次加法运算。

此种方法的时间复杂度是 $O(K^2)$。

**代码实现参考（Java语言）**
方法二是对方法一的优化，也即在方法二中我们舍去了方法一种的 `sum()` 函数。在第二重循环中用上一次循环的值即$S(p,q)$，加上当前位置的数值即 $S(p,q) + a_{q+1}$。完整代码如下：

```java
public class MaxSubsequence2 {

    public static void main(String[] args) {
        double[] arrs = {1.5, -12.3, 3.2, -5.5, 23.2, 3.2, -1.4, -12.2, 34.2, 5.4, -7.8, 1.1, -4.9};
        MaxSubsequence2 maxSubsequence = new MaxSubsequence2();
        Subsequence subsequence = maxSubsequence.findOut(arrs);
        subsequence.print();
    }

    public Subsequence findOut(double[] nums) {
        double max = Double.MIN_VALUE;
        int start = -1;
        int end = -1;
        for (int i = 0; i < nums.length; i++) {
            double sum = 0;
            for (int j = i; j < nums.length; j++) {
                sum += nums[j];
                if (sum > max) {
                    max = sum;
                    start = i;
                    end = j;
                }
            }
        }
        return new Subsequence(nums, start, end, max);
    }
}
```

## 方法三 利用分治（Divide-and-Conquer）算法

首先，将序列一分为二，分成从 $1 \text{\textasciitilde} \displaystyle\frac{K}{2}$，以及 $\displaystyle\frac{K}{2}+1 \text{\textasciitilde} K$两个子序列。然后，对这两个子序列分别求它们的最大区间。接下来有两种情况

- 前后两个子序列的总和最大区间中间没有间隔，也就是说前一个最大区间是 $[p, \displaystyle\frac{K}{2}]$ 后一个是 $[\displaystyle\frac{K}{2}+1,q]$，如果两个区间的和均为正整数，则最大区间为 $[p,q]$，否则就选取两个区间中最大的一个。
- 前后两个子序列的总和最大区间中间有间隔，假定这两个子序列的总和最大区间分别为 $[p_1,q_1]$ 和 $[p_2,q_2]$。这时，整个序列的总和最大区间是下面三者中最大的那一个。
  * $[p_1,q_1]$
  * $[p_2,q_2]$
  * $[p_1,q_2]$

![](/images/jisuanzhihun/a1-2.3.1.png)

此种算法的时间复杂度是 $O(K\log_2{K})$

**代码实现参考（Java语言）**

首先我们定义一个方法来处理序列一分为二后，计算得到的子序列。

## 方法四 正、反两遍扫描法

在方法2中，我们是先设定区间的左边界 $p$，在此条件下确定总和最大区间的右边界$q$。然后再改变左边界，测试所有的可能性。

我们从这个想法出发，寻找一下线性负责度，及 $O(K)$ 的算法，步骤如下

1. 先在序列中找到第一个不为0的正数。

* 如这个数不存在，也即 $\forall a_i \le 0$。那么区间中最大的那个数就是要找的区间。这时算法的复杂度是 $O(K)$
* 若 $\exist a_i > 0$。则从头部开始删除直到遇到第一个大于0的数。

2. 用类似于方法2中的做法，先把左边界固定在第一个数，然后让 $q=2,3,4,\dots,K$，计算 $S(1,q)$，以及到目前为止的最大值$Maxf$ 和达到最大的右边界 $r$.
3. 如果对于所有的 $q$ ，都有 $S(1,q) >= 0$，或存在某个 $q_0$，当 $q > q_0$，上述条件满足，这个情况比较简单。当扫描到最后，即$q=K$ 时，所保留的那个 $Maxf$所对应的 $r$ 就是我们要找的区间的右边界。

# 总结
